define(function(){
    'use strict';
    return {
        rand: function(from, to) {
            return Math.min(Math.ceil(from + Math.random() * to), to);
        }
    };
});