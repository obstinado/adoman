define(function(){
    'use strict';
    function EventDispatcher(){
        this.listCallbacks = {};
        this.getCallbacks = function(type){
            if(!this.listCallbacks[type]){
                this.listCallbacks[type] = jQuery.Callbacks();
            }

            return this.listCallbacks[type];
        };
        this.publish = function(type){
            this.getCallbacks(type).fire(Array.prototype.slice.call(arguments, 1));
            return this;
        };
        this.subscribe = function(type){
            this.getCallbacks(type).add(Array.prototype.slice.call(arguments, 1));
            return this;
        };
        this.unsubscribe = function(type){
            this.getCallbacks(type).remove(Array.prototype.slice.call(arguments, 1));
            return this;
        };
    }
    return EventDispatcher;
});