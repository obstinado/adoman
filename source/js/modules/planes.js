define(['modules/eventdispatcher', 'modules/utils', 'modules/filters', 'modules/rof'], function(EventDispatcher, utils, filters){
    'use strict';
    var defaultOptions = {
            MAX_PLANE_WIDTH: 150,
            MAX_PLANE_HEIGHT: 200,
            MIN_PLANE_WIDTH: 50,
            MIN_PLANE_HEIGHT: 60,
            width: 1000,
            height: 300,
            sceneWidth: 500,
            duration: 800,
            gradient: [[0, '#a9d4ea'], [0.005, '#a9d4ea'], [0.006, '#80c2e1'], [0.2, '#4bafd7'], [0.3, '#43abd2'], [0.5, '#48aed6'], [0.6, '#4bb0d7'], [0.8, '#3d9ecd'], [0.9, '#40a1cf'], [1, '#409fcf']]
        };


    function Planes(options){
        var _this = this;
        this.canvas = document.createElement('canvas'),
        this.ctx = this.canvas.getContext('2d');
        this.options  = $.extend({}, defaultOptions, options);
        this.canvas.width = this.options.width || document.body.offsetWidth;
        this.canvas.height = this.options.height || 300;
        this.planeWidth = this.options.MAX_PLANE_WIDTH;
        this.vertices = [[], []];
        this.animating = false;
        $.extend(this, new EventDispatcher);
        
        if(this.options.coords){
            this.fromData(this.options.coords);
            //this.updateVertices();
        }else{
            $.each([
                [this.options.width/2-this.options.sceneWidth/2, 0],
                [this.options.width/2+this.options.sceneWidth/2, this.options.width]], 
            function(){
                _this.getVertices(this[0], this[1]);
            });
        }
        
        this.vertices[0].sort(this.sortVerticesXAxis);
        this.vertices[1].sort(this.sortVerticesXAxis);
    }
    Planes.prototype = {
        getVertices: function(start, end){
            var x = start,
                op = (end > start) ? 1 : -1;
            this.vertices[0].push([start, utils.rand(0, 50)]);
            this.vertices[1].push([start, this.options.height - utils.rand(0, 50)]);
            while(true){
                var dx = utils.rand(10, 20);
                x = x + op*(this.planeWidth + dx);
                this.vertices[0].push([x, utils.rand(0, this.options.height / 2)]);
                this.vertices[1].push([x, this.options.height - utils.rand(0, this.options.height / 2)]);
                if(start>end && x<end || end>start && x>end){
                    break;
                }
            }
        },
        fromData: function(coords){
            var center = this.options.width / 2,
                _this = this,
                len = coords[0].length;
            $.each(coords[0], function(i){
                var x = (i == len - 1) ? _this.options.width : this[0];
                _this.vertices[0].push([_this.options.width/2 + x, this[1]]);
                _this.vertices[1].push([_this.options.width/2 + x, coords[1][i][1]]);
            });
            this.updateBoundVertices();
        },
        sortVerticesXAxis: function(a, b){
            if(a[0]<b[0])
                return -1;
            if(a[0]>b[0])
                return 1;
            return 0;
        },
        createGradient: function(points, stops){
            var g = this.ctx.createLinearGradient(points[0], points[1], points[2], points[3]);
            $.each(stops, function(){
                g.addColorStop(this[0], this[1]);
            });
            return g;
        },
        drawGradientOnPlane: function(points, gradient, alpha){
            this.ctx.save();
            this.ctx.globalAlpha = (!alpha) ? 1 : alpha;
            this.ctx.fillStyle = gradient;
            this.ctx.beginPath();
            this.ctx.moveTo(points[0][0], points[0][1]);
            this.ctx.lineTo(points[1][0], points[1][1]);
            this.ctx.lineTo(points[2][0], points[2][1]);
            this.ctx.lineTo(points[3][0], points[3][1]);
            this.ctx.lineTo(points[0][0], points[0][1]);
            this.ctx.fill();
            this.ctx.closePath();
            this.ctx.restore();
        },
        /*
            Возвращяет индекс точки, ближайшей к центру по горизонтали
            Точки отсортированы по x координаты 
            @return integer
        */
        getVertexIndexBeforeCenter: function(){
            for(var i=0,len=this.vertices[0].length;i<len;i++){
                if(this.vertices[0][i][0] > this.options.width/2){
                    return i-1;
                }
            }
            return -1;
        },
        getRandX: function(){
            return (this.planeWidth + utils.rand(10, 20));
        },
        updateBoundVertices: function(){
            var dx, len = this.vertices[0].length,
                startX = this.vertices[0][0][0], endX = this.vertices[0][len-1][0];
            if(this.vertices[0][0][0] > 0){
                while(startX > 0){
                    startX -= this.planeWidth;
                    this.vertices[0].splice(0,0,[startX, utils.rand(10, 20)]);
                    this.vertices[1].splice(0,0,[startX, utils.rand(100, 200)]);
                }
            }
            if(this.vertices[0][len-1][0] < this.options.width){
                while(endX < this.options.width){
                    endX += this.sceneWidth;
                    this.vertices[0].splice(this.vertices[0].length,0,[endX, utils.rand(10, 20)]);
                    this.vertices[1].splice(this.vertices[1].length,0,[endX, utils.rand(100, 200)]);    
                }
            }
        },
        /*
            Если за областью видимости слева нет точек, то добавляем
            Удаляем ненужные справа
        */
        updateVertices: function(dir){
            if(dir > 0){
                this.vertices[0].splice(0,0,[this.vertices[0][0][0], this.vertices[0][0][1]]);
                this.vertices[1].splice(0,0,[this.vertices[0][0][0], this.vertices[1][0][1]]);
            }else{
                var len = this.vertices[0].length;
                this.vertices[0].splice(len,0,[this.vertices[0][len-1][0], this.vertices[0][len-1][1]]);
                this.vertices[1].splice(len,0,[this.vertices[0][len-1][0], this.vertices[1][len-1][1]]);
            }
        },
        go: function(dir){
            var dir = (typeof dir == 'undefined') ? 1 : dir;
            this.nextState(dir);
            this.updateVertices(dir);
            this.animate(this.options.duration);
        },
        nextState: function(dir){
            var vertices = this.vertices;
            for(var i=0,len=vertices[0].length;i<len;i++){
                if(i==0 && dir<0 || i==len-1 && dir>0){ continue; }
                vertices[0][i]["toPosition"] = [vertices[0][i+dir][0], vertices[0][i+dir][1]];
                vertices[1][i]["toPosition"] = [vertices[1][i+dir][0], vertices[1][i+dir][1]];
            }
        },
        copyVertices: function(ar){
            var orig = [[],[]];
            for(var i=0,len=this.vertices[0].length;i<len;i++){
                orig[0].push([this.vertices[0][i][0], this.vertices[0][i][1]]);
                orig[1].push([this.vertices[1][i][0], this.vertices[1][i][1]]);
            }
            return orig;
        },
        animate: function(duration){
            this.publish('animationStart');
            this.animating = true;
            var _this = this,
                start = new Date().getTime(),
                end = start + duration,
                orig = this.copyVertices();
            
            (function step() {
                var timestamp = new Date().getTime();
                var progress = Math.min((duration - (end - timestamp)) / duration, 1);
                for(var i=0,len=_this.vertices[0].length;i<len;i++){
                    if(typeof _this.vertices[0][i]["toPosition"] == "undefined"){
                        continue;
                    }
                    _this.vertices[0][i][0] = orig[0][i][0] + (_this.vertices[0][i]["toPosition"][0]-orig[0][i][0]) * progress;
                    _this.vertices[0][i][1] = orig[0][i][1] + (_this.vertices[0][i]["toPosition"][1]-orig[0][i][1]) * progress;
                    _this.vertices[1][i][0] = orig[1][i][0] + (_this.vertices[1][i]["toPosition"][0]-orig[1][i][0]) * progress;
                    _this.vertices[1][i][1] = orig[1][i][1] + (_this.vertices[1][i]["toPosition"][1]-orig[1][i][1]) * progress;
                }

                _this.draw();

                if (progress < 1){
                    window.requestAnimationFrame(step);
                }else{
                    orig = null;
                    _this.animating = false;
                    _this.publish('animationEnd');
                }
            })();
        },
        drawPoint: function(x, y, index){
            this.ctx.beginPath();
            this.ctx.arc(x, y, 3, 0, 2 * Math.PI, false);
            this.ctx.fillStyle = 'black';
            this.ctx.fill();
            this.ctx.fillText(index, x+5, y+5);
        },
        draw: function(){
            // Обновляем сцену
            if(this.options.backgroundColor){
                this.ctx.fillStyle = this.options.backgroundColor;
                this.ctx.fillRect(0, 0, this.options.width, this.options.height);
            }else{
                this.ctx.clearRect(0, 0, this.options.width, this.options.height);
            }
            for(var i=0,len=this.vertices[0].length;i<len;i++){
                if(i+1 < len){
                    var g1 = this.createGradient([this.vertices[0][i][0], (this.vertices[1][i][1]-this.vertices[0][i][1])/2+this.vertices[0][i][1], this.vertices[0][i+1][0], (this.vertices[1][i][1]-this.vertices[0][i][1])/2+this.vertices[0][i][1]], this.options.gradient);
                    this.drawGradientOnPlane([this.vertices[0][i], this.vertices[0][i+1], this.vertices[1][i+1], this.vertices[1][i]], g1, 0.96)
                }
                // this.drawPoint(this.vertices[0][i][0], this.vertices[0][i][1], ''+i+'('+this.vertices[0][i][0]+','+this.vertices[0][i][1]);
                // this.drawPoint(this.vertices[1][i][0], this.vertices[1][i][1], ''+i+'('+this.vertices[1][i][0]+','+this.vertices[1][i][1]);
            }
            this.publish('afterDraw');
        }
    };

    return Planes;
});