define(['modules/eventdispatcher'], function(EventDispatcher){
    'use strict';
    var container = $('.promo-blocks'),
        blocks = container.find('.block'),
        current = 1,
        mouseOver = false,
        animating = false,
        timerID = null;

    var Carousel = {
        init: function(){
            var _this = this,
                canvasWidth = $('.top-scene').width() || 1000;
            $('.promo-row')
                    .on('click', function(evt){
                        var dir = (evt.clientX <= canvasWidth/2) ? -1 : 1;
                        clearTimeout(timerID);
                        _this.go(dir);
                    });
            timerID = setTimeout($.proxy(this.go, this, 1), 5000);
        },
        go: function(dir){
            if(animating){ return; }
            animating = true;
            current = (blocks.size() == current) ? 1 : (blocks.size() == 1 && dir < 0) ? blocks.size() : current + Math.abs(dir);
            blocks.filter('.active').fadeOut(100);
            this.publish('go', dir);
        },
        show: function(){
            blocks.eq(current-1).fadeIn(100, function(){
                animating = false;
                blocks.eq(current-1).addClass('active');
            });
            timerID = setTimeout($.proxy(this.go, this, 1), 5000);
        }
    };
    $.extend(Carousel, new EventDispatcher);

    return Carousel;
});