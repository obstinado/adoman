require(['modules/planes', 'modules/filters', 'modules/carousel'], function(Planes, filters, carousel){
    'use strict';
    var coords = {
                front: {
                    normal: [[[-661,170],[-618,188],[-401,155],[-273,23],[298,44],[461,85],[493,64],[651,91]],[[-661,335],[-618,312],[-401,212],[-273,260],[298,387],[461,250],[493,286],[651,259]]],
                    condensed: [[[-661,85],[-618,90],[-401,75],[-273,12],[298,22],[461,42],[493,32],[651,45]],[[-661,160],[-618,150],[-401,106],[-273,130],[298,190],[461,125],[493,143],[651,125]]]
                },
                back: {
                    normal: [[[-651,80],[-456,141],[-345,159],[-153,138],[326,10],[451,141],[651,171]],[[-651,360],[-456,370],[-345,238],[-153,373],[326,361],[451,251],[651,261]]],
                    condensed: [[[-651,40],[-456,70],[-345,80],[-153,70],[326,5],[451,70],[651,85]],[[-651,180],[-456,185],[-345,120],[-153,160],[326,80],[451,125],[651,130]]]
                }
            },
        plane_type = $('.promo-row').hasClass('condensed') ? 'condensed' : 'normal';
    
    var back_plane = new Planes({width: 1400, height: 370, coords: coords.back[plane_type], backgroundColor: '#FFFFFF', gradient: [[0, '#b6cb42'], [0.4, '#8fb217'], [0.8, '#9fbd30'], [1, '#a9c241']]});
    back_plane.subscribe('afterDraw', function(){
        filters.blur(back_plane.canvas, 0, 0, back_plane.options.width, back_plane.options.height, 3);
    });
    back_plane.draw();

    var plane = new Planes({width: 1400, height: 400, coords: coords.front[plane_type]})
    plane.subscribe('afterDraw', function(){
        filters.noise(plane.canvas, 0.035);
    });
    plane.draw();
    

    $(function(){
        var $backScene = $(back_plane.canvas)
                            .appendTo('#planes')
                            .addClass('back-scene')
                            .wrap($('<div class="back_wrap plane"/>')),
            $topScene = $(plane.canvas)
                            .appendTo('#planes')
                            .addClass('top-scene')
                            .wrap($('<div class="top_wrap plane"/>'));
        carousel.init();
        carousel.subscribe('go', function(direction){
            plane.go(direction[0]);
        });
        plane.subscribe('animationEnd', $.proxy(carousel.show, carousel));
        $('.promo-blocks').removeClass('is-hidden');
    });
});