## Настройка
В проекте применяется gruntjs, для его работы требуется установить nodejs (npm идет вместе с последней версией)

### Установка nodejs
<http://nodejs.org/download/>

### Gruntjs
<http://gruntjs.com/getting-started>

### Compass (+ Ruby)
<http://compass-style.org/install/>

Устанавливаем пакеты `npm install`
Запускаем сервер `python -m SimpleHTTPServer`

## Работа с файлами
Соотвественно, если вы редактируете файлы в папке source, рекомендую запускать `grunt watch`.
Отдельная команда `grunt deploy`

## Структура
source/sass - sass стили, конвертируются в css compass'ом в static/js
source/js - исходные скрипты, собираются r.js (<http://requirejs.org/docs/optimization.html>) и кладутся в statis/js

На страницы загружется веб-шрифт из static/fonts (см. source/sass/_fonts.scss)

## Примечание
Возможно, для формочек потребуется оформить вывод ошибок=)

## Контакты для замечаний, пожеланий
Алексей obstinado@ya.ru