module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    watch: {
        sass: {
            files: ['source/sass/*.scss'],
            tasks: ['compass:prod', 'csso:prod']
        },
        js: {
            files: ['source/js/**/*.js'],
            tasks: ['requirejs', 'concat:js', 'clean:js']
        }
    },
    requirejs: {
        compile: {
            options: {
              appDir: 'source/js',
              dir: 'static/js',
              baseUrl: '.',
              removeCombined: true,
              modules: [
                {
                  name: 'apps/index'
                }
              ]
            }
        }
    },
    csso: {
        prod: {
            files: {'static/css/default.css': 'static/css/default.css'}
        }
    },
    compass: {
        prod: {
            options: {
                config: 'source/sass/config.rb',
                cssDir: 'static/css',
                sassDir: 'source/sass'
            }
        }
    },
    clean: {
        js: ['static/js/build.txt', 'static/js/config.js', 'static/js/require.js']
    },
    concat: {
        options: {
            nonull: true,
            stripBanners: true,
            banner: '/*! <%= grunt.template.today("yyyy-mm-dd") %> */'
        },
        js: {
            src: ['static/js/config.js', 'static/js/require.js', 'static/js/jquery-2.1.0.min.js'],
            dest: 'static/js/require_jquery.js'
        }
    },
    uncss: {
        dist: {
            files: {
                'static/css/defualt-tidy.css': ['index.html', 'feedback.html']
            }
        }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-requirejs');
  grunt.loadNpmTasks('grunt-csso');
  grunt.loadNpmTasks('grunt-uncss');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-clean');

  grunt.registerTask('deploy', ['compass:prod', 'csso:prod', 'requirejs', 'concat', 'clean']);
};